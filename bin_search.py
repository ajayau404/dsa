
def locate_num(search_list, mid, search_key):
    if search_key == search_list[mid]:
        if mid > 0:
            if search_list[mid-1] == search_key:
                return "left"
        return "found"
    elif search_key > search_list[mid]:
        return "left"
    else:
        return "right"

def binary_search(search_list, search_key):
    """
    return the first occurance of the number
    """
    start, end = 0, len(search_list)-1
    while start <= end:
        mid = (start + end)//2
        print(f"start:{start}, end:{end} mid:{mid}")
        ret_location = locate_num(search_list, mid, search_key)
        if ret_location == "found":
            return mid
        elif ret_location == "left":
            end = mid -1
        else:
            start = mid + 1

    return -1