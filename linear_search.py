
def linear_search(search_list, search_key):
    for index, val in enumerate(search_list):
        if val == search_key:
            return index
    return -1

def lin_search(search_space, search_elem):
    index = 0
    while index < len(search_space):
        if search_elem == search_space[index]:
            return index
        index += 1
    return -1        

def main():
    search_list = [5,4,3,2,1]
    search_elem = 3
    search_res = lin_search(search_list, search_elem)
    print("search_res:", search_res)

if __name__ == "__main__":
    main()