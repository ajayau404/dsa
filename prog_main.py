from linear_search import linear_search, lin_search
from bin_search import binary_search
def main():
    search_list = [3,3,3,2,1]
    search_elem = 3
    # search_res = lin_search(search_list, search_elem)
    search_res = binary_search(search_list, search_elem)
    print("search_res:", search_res)

if __name__ == "__main__":
    main()