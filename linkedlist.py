

class Node(object):
    """
    Keep the structure of the Node here
    """
    def __init__(self, data: int):
        """
        data -> contains
        link -> reference to the next node
        """
        self.data = data
        self.link = None

    def __repr__(self):
        return str(self.data)
    
    def __str__(self):
        return str(self.data)

    # def __iter__(self):
    #     # print("Iteratior")
    #     return self

    # def __next__(self):
    #     """
    #     Not working
    #     """
    #     # print(f"next self:{self} {self.link}")
    #     if self == None:
    #         raise StopIteration
    #     else:
    #         temp = self
    #         self = self.link
    #         return temp

    #     # raise StopIteration

class LinkedList(object):
    """
    Contain all the different methods of linked list
    """
    def __init__(self):
        self.head = None

    def insert(self, data: int, position: int):
        """
        Insert at a specific position of the list
        If trying to insert to invalid position exception occurs.
        """
        new_node = Node(data)
        if position == 0:
            new_node.link = self.head
            self.head = new_node
        else:
            temp_node = self.head
            for node_iter in range(position-1):
                if temp_node == None:
                    raise Exception("Cannot insert at given position")
                temp_node = temp_node.link
            if not temp_node:
                raise Exception("Cannot insert")
            new_node.link = temp_node.link
            temp_node.link = new_node
    
    def print_ll(self):
        """
        Prints the list forward direction iteratively
        """
        temp_node = self.head
        while temp_node != None:
            print(f"{temp_node.data}", end="->")
            temp_node = temp_node.link
        print()

    def print_ll_recursive(self, head):
        """
        Prints linked list using recursion
        """
        if head == None:
            return
        print(head, end="->")
        self.print_ll_recursive(head.link)

    def print_ll_recursive_rev(self, head):
        """
        Prints the linked list in reverse order using recursion
        """
        if head == None:
            return
        self.print_ll_recursive_rev(head.link)
        print(head, end="->")

    def reverse_ll_iter(self):
        """
        Reverse the linked list iteratively
        """
        prev_n, current_n, n_n = None, self.head, self.head
        while current_n != None:
            # print(f"current_n.link :{current_n.link}")
            n_n = current_n.link
            current_n.link = prev_n
            prev_n = current_n
            current_n = n_n
        self.head = prev_n

    def reverse_ll_recursive(self, head):
        """
        Reverse the linked list using recursion
        """
        if head.link == None:
            self.head = head
            return
        self.reverse_ll_recursive(head.link)
        head.link.link = head
        head.link = None
        

def main():
    ll = LinkedList()

    ll.insert(1, 0)
    ll.insert(2, 1)
    ll.insert(3, 2)
    ll.insert(4, 3)
    # ll.insert(5, 0)
    # ll.insert(8, 3)
    # ll.insert(8, 8)
    
    ll.print_ll()
    ll.print_ll_recursive(ll.head)
    print()
    ll.print_ll_recursive_rev(ll.head)
    print()
    ll.reverse_ll_iter()
    ll.print_ll()
    ll.reverse_ll_recursive(ll.head)
    ll.print_ll()
    # ll.print_ll_recursive_rev(ll.head)

if __name__ == "__main__":
    main()